using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour
{
    [SerializeField] GameObject[] objectsToOff;
    [SerializeField] GameObject gameOverPanel;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            gameOverPanel.SetActive(true);
            foreach (GameObject obj in objectsToOff)
            {
                obj.SetActive(false);
            }
        }
    }
}
