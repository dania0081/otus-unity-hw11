﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointToPointMove : MonoBehaviour
{
    public Transform[] points;
    public float speed = 2f;
    public float waitTime = 3f;
    private int currentPointIndex = 0;
    private bool movingForward = true;

    private void Start()
    {
        if (points.Length > 0)
        {
            transform.position = points[0].position;
        }
        else
        {
            Debug.LogWarning("Unused script! No points to move.");
            enabled = false;
        }
        
    }

    private void Update()
    {
        if (points.Length > 0)
        {
            if (movingForward)
            {
                transform.position = Vector3.MoveTowards(transform.position, points[currentPointIndex].position, speed * Time.deltaTime);
            }

            if (Mathf.Approximately(Vector3.Distance(transform.position, points[currentPointIndex].position), 0))
            {
                if (!IsInvoking("Waiting"))
                {
                    Invoke("Waiting", waitTime);
                }
            }
        }
    }

    private void Waiting()
    {
        currentPointIndex++;
        if (currentPointIndex >= points.Length)
        {
            currentPointIndex = 0;
        }
    }
}
